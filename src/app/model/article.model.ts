export class Article {

  public isvisible: boolean;
  public title: string;
  public image: string;
  public text: string;
}
