import { ServiceService } from './../service/service.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-article-container',
  templateUrl: './article-container.component.html',
  styleUrls: ['./article-container.component.css']
})
export class ArticleContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  showArticleDetail() {
    // fonction qui va chercher l'article dans le service
  }

}
